## Multiplicity Check
This project is designed to check the S1/S2 multiplicity for selected events. There are two methods for checking multiplicity:
1) S1/S2 hit vector given by 'Event/S1hits' or 'Event/S2hits' in the root file.
2) Analyzing raw waveform. For each event, the raw waveform for each channel is passed to S1/S2 filter and compared to the S1/S2 threshold. If crossing the threshold, its contribution to the multiplicity array for the event is considered properly. 

### There are two files in this project:
#### 1) MultiplicityCheck.py 
The class used to do all the multiplicity checks. The location of the salted root file is required when initializing the class. There are four functions used to do the checks:
* Function S1HitVector(): gives OD S1 multiplicity by using S1 hit vector.
* Function S2HitVector(): gives TPC S2 multiplicity by using S2 hit vector.
* Function S1Multiplicity(S1Threshold=3000, S1Mulexpected=15, S1Coinci=32, S1width=15,CaptureWindow=6): gives OD S1 multiplicity by analyzing raw waveform and the list of channels contributed to the multiplicity for the event. Information about threshold, multiplicity, coincidence window, filter width can be found in run catalog in run table and should not be changed during SR2. CaptureWindow is defined as the additional time we want to include after the mulitplicity is achieved. The default value for CaptureWindow is 6 samples. This value can be adjusted if needs.
* Function S2Multiplicity(S2Threshold, S2Mulexpected, S2Coinci=500, S2width=125): gives TPC S2 multiplicity by analyzing raw waveform and the list of channels contributed to the multiplicity for the event. Information about threshold, multiplicity, coincidence window, filter width can be found in run catalog in run table and should not be changed during SR2. CaptureWindow is defined as the additional time we want to include after the mulitplicity is achieved. The default value for CaptureWindow is 6 samples. With 6 samples CaptureWindow, the multiplicity given by analyzing raw waveform theorically should match what given by S2 hit vector. However, this method doesn't include filter noise, so the trace after S2 filter may be smaller than the actual trace. CaptureWindow can be adjusted to compensate the discrepancy caused by filter noise. 

For checking multiplicity by raw waveform, trigger time is required for analyzing. In the root file, only the trigger time of the event trigger is recorded. Then, if the event trigger for one event is TPC S2, its OD trigger time will be unknown. So, we cannot apply multiplicity by raw waveform to this event. For example, in 1202SaltedEventsCheck.ipynb, the multiplicity by raw waveform for OD S1 is not presented since all events in this check have TPC S2 as their event trigger.

In doing multiplicity analysis, all information comes from the root file including event number, S1/S2 hits, channel number, trigger time, start time of raw waveform and zData of the raw waveform. If the information is wrong, the results of the analysis will be wrong. For example, if the S1/S2 hits is not properly recorded in the root file, the results of S1/S2 multiplicity by hit vector will be weird.

#### 2) 1202SaltedEventsCheck.ipynb 
An example about how to do multiplicity check with the class. 

If there are questions, feel free to reach Yufan (yqie2@u.rochester.edu).
