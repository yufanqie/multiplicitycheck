import uproot
import numpy as np
import pandas as pd


class Multiplicity:
    
    global OD, TTPC, S1timeRange, S2preWin, S2postWin, TTPC_DDCIDList,OD_DDCIDList
    
    OD = [800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820,
              821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832,
              833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853,
              854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865,
              866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886,
              887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898,
              899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919,
              920]
    TTPC = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
                28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
                43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68,
                69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83,
                84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107,
                108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
                120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
                141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152,
                153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173,
                174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185,
                186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206,
                207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219,
                220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240,
                241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252]
    TTPC_DDCIDList = [[34, 16, 10, 17, 35, 26, np.nan, 25, 12, 30, 29, 36, 28, 27, 11, 18, 8, 2, 0, 6, 5, 4, 9, 3, 51, 41, 52, 60, 82, 70, 69, 81], [123, 135, 147, 111, 99, 63, 92, 64, 215, 214, 213, 212, 195, 171, 183, 159, 217, 216, 196, 124, 100, 112, 136, 160, 218, 184, 172, 148, 197, 221, 220, 219], [89, 117, 153, 189, 238, 237, 177, 152, 105, 95, 129, 202, 240, 239, 165, 141, 106, 118, 142, 190, 242, 241, 166, 130, 90, 119, 155, 203, 244, 243, 178, 154], [233, 164, 188, 176, 201, 236, 235, 234, 68, 50, 59, 80, 116, 140, 128, 104, 139, 103, 94, 127, 200, 232, 231, 163, 150, 88, 79, 49, 40, 67, 115, 151], [7, 19, 43, 73, 55, 31, 13, 1, 21, 32, 44, 62, 61, 37, 20, 14, 194, 208, 207, 157, 133, 97, 91, 121, 181, 206, 205, 252, 251, 180, 193, 169], [96, 131, 191, 246, 245, 179, 143, 107, 192, 250, 249, 248, 247, 167, 204, 168, 54, 84, 72, 71, 83, 53, 42, np.nan, 109, 145, 156, 144, 132, 108, 120, 85], [149, 87, 76, 77, 101, 125, 137, 113, 146, 110, 98, 74, 56, 45, 75, 86, 222, 173, 185, 161, 198, 225, 224, 223, 211, 210, 209, 158, 122, 134, 182, 170], [57, 46, 38, np.nan, 22, 15, 23, 33, 65, 47, 39, 24, 48, 58, 78, 66, 226, 162, 126, 93, 102, 114, 138, 186, 227, 174, 199, 175, 187, 230, 229, 228]]
    
    OD_DDCIDList = [[824, 836, 861, 869, 873, 884, 912, np.nan, 822, 830, 867, 863, 879, 906, 910, np.nan, 820, 828, 832, 865, 877, 900, 908, 916, 826, 834, 838, 871, 875, 902, 914, 918], [800, 808, 816, 845, 853, 880, 888, 896, 802, 810, 818, 847, 855, 882, 890, 898, 804, 812, 841, 849, 857, 904, 872, np.nan, 806, 814, 843, 851, 859, 886, 894, np.nan], [801, 809, 817, 844, 852, 881, 889, 897, 803, 811, 819, 846, 854, 883, 891, 899, 805, 813, 840, 848, 856, 885, 893, np.nan, 807, 815, 842, 850, 858, 887, 895, np.nan], [825, 837, 860, 868, 892, 905, 913, np.nan, 827, 831, 839, 862, 874, 907, 915, np.nan, 821, 829, 833, 864, 876, 901, 909, 917, 823, 835, 866, 870, 878, 903, 911, 919]]

    S1timeRange = 200
    S2preWin = 500
    S2postWin = 500
    
    def __init__(self, saltedRootFile):

        global file, evts, trgTime, evtarray, channelarray, timearray, zDataarray
        
        file = uproot.open(saltedRootFile)

        
        evts = np.array(file['Event']['globalEvt'])
        trgTime = np.array(file['Event']['trgTimeStamp'])

        evtarray = np.array(file['Data']['evt'])
        channelarray = np.array(file['Data']['channel'])
        timearray = np.array(file['Data']['startTime'])
        zDataarray = np.array(file['Data']['zData'])

    def S1HitVector(self):
        # calculate OD S1 multiplicity by using S1 hit vector
        # S1 hit vector is given by 'Event/S1hits' in the root file
        S1Info = np.array(file['Event']['S1hits'])
        OD_DDCID = [37, 39, 41, 43]
        S1hitsArray = []
        S1hitsChannel = []
        for S1hitt in S1Info:
            temhit = []
            temMul = 0
            for a in OD_DDCID:
                temhit.append(S1hitt[a])
            s1hit = self.HitVectorBinary(temhit)
            temCh = []
            for j in range(len(s1hit)):  # go through the 8 digitizers
                for k in range(len(s1hit[j])):  # go through all chs in the digitizer
                    if (s1hit[j][k] > 0):
                        temMul += 1
                        temCh.append(OD_DDCIDList[j][k])
            S1hitsArray.append(temMul)
            S1hitsChannel.append(temCh)
        return S1hitsArray,S1hitsChannel

    def S2HitVector(self):
        # calculate TPC S2 multiplicity by using S2 hit vector
        # S2 hit vector is given by 'Event/S2hits' in the root file
        S2Info = np.array(file['Event']['S2hits'])
        TTPC_DDCID = [16, 17, 18, 22, 23, 24, 28, 29]
        S2hitsArray = []
        S2hitsChannel = []
        for S2hitt in S2Info:
            temhit = []
            temMul = 0
            for a in TTPC_DDCID:
                temhit.append(S2hitt[a])
            # print(temhit)
            s2hit = self.HitVectorBinary(temhit)
            temCh = []
            for j in range(len(s2hit)):  # go through the 8 digitizers
                for k in range(len(s2hit[j])):  # go through all chs in the digitizer
                    if (s2hit[j][k] > 0):
                        temMul += 1
                        temCh.append(TTPC_DDCIDList[j][k])
            S2hitsArray.append(temMul)
            S2hitsChannel.append(temCh)
        return S2hitsArray,S2hitsChannel

    def S1Multiplicity(self,S1Threshold=3000, S1Mulexpected=15, S1Coinci=32, S1width=15,CaptureWindow=6):
        # function to calculate S1 multiplicity through analyzing raw waveform

        S1multiplicity = []
        ContributedChs = []
        for i in range(len(evts)):
            evt = evts[i]
            evtmask = evtarray == evt
            channels = channelarray[evtmask]
            times = timearray[evtmask]
            zDatas = zDataarray[evtmask]
            
            PMTs = np.unique(channels)

            current_triggerTime = int(trgTime[i])
            current_mul = np.zeros(int(S1timeRange * 2))
            
            ChannelAllRecords = []
            for add in range(len(current_mul)):
                ChannelAllRecords.append([-1])
            
            for j in range(len(PMTs)):

                if (PMTs[j] in OD):
                    
                    channelmask = channels==PMTs[j]
                    timesPMT = times[channelmask]
                    zDatasPMT = zDatas[channelmask]

                    current_PMT_trace_t0 = timesPMT[0]

                    if (abs(current_PMT_trace_t0 - current_triggerTime) < S1timeRange):
                        current_Pod_trace = zDatasPMT[0]

                        S1_trace = self.trace_S1_filter(current_Pod_trace, S1width)

                        current_mul_prev = current_mul
                        current_mul = self.S1MultiplicityUpdate(S1_trace, current_triggerTime, current_PMT_trace_t0,
                                                           S1Threshold, current_mul, S1Coinci, S1timeRange, 0)
                        for ChStore in range(len(current_mul)):
                            if(current_mul[ChStore]!=current_mul_prev[ChStore]):
                                ChannelAllRecords[ChStore].append(PMTs[j])              
                
            
            CaptureInd = np.argwhere(current_mul>=S1Mulexpected)
            if(len(CaptureInd)==0):
                S1multiplicity.append(max(current_mul))
                CaptureInd = np.argmax(current_mul)
                ContributedChs.append(ChannelAllRecords[CaptureInd][1:])
            else:
                CaptureInd = CaptureInd[0][0]+CaptureWindow
                S1multiplicity.append(max(current_mul[:int(CaptureInd+1)]))
                ContributedChs.append(ChannelAllRecords[int(CaptureInd)][1:])
            
        return S1multiplicity,ContributedChs

    def S2Multiplicity(self,S2Threshold=1150, S2Mulexpected=6, S2Coinci=500, S2width=61,CaptureWindow=6):
        # function to calculate S1 multiplicity through analyzing raw waveform

        S2multiplicity = []
        ContributedChs = []
        for i in range(len(evts)):

            evt = evts[i]
            evtmask = evtarray==evt
            channels = channelarray[evtmask]
            times = timearray[evtmask]
            zDatas = zDataarray[evtmask]
            
            PMTs = np.unique(channels)

            current_triggerTime = int(trgTime[i])
            current_mul = np.zeros(int(S2preWin+S2postWin+S2Coinci-S2width*6))
            
            ChannelAllRecords = []
            for add in range(len(current_mul)):
                ChannelAllRecords.append([-1])
            
            for j in range(len(PMTs)):

                if(PMTs[j] in TTPC):

                    channelmask = channels==PMTs[j]
                    timesPMT = times[channelmask]
                    zDatasPMT = zDatas[channelmask]

                    CurrentWaveform = np.zeros(int(S2preWin+S2postWin))
                    CurrentWaveform_start = current_triggerTime - int(S2preWin)
                    CurrentWaveform_end = current_triggerTime + int(S2postWin)

                    for k in range(len(zDatasPMT)):

                        current_Pod_trace = zDatasPMT[k]
                        current_PMT_trace_start = timesPMT[k]
                        current_PMT_trace_end = current_PMT_trace_start+len(current_Pod_trace)
                        if (current_PMT_trace_start >= CurrentWaveform_end): continue
                        if (current_PMT_trace_end <= CurrentWaveform_start): continue
                        mean = np.mean(current_Pod_trace[0:25])
                        current_Pod_trace = [x - mean for x in current_Pod_trace]
                        if current_PMT_trace_start >= CurrentWaveform_start:
                            index_POD_start = 0
                            index_WaveForm_start = current_PMT_trace_start - CurrentWaveform_start
                        else:
                            index_POD_start = CurrentWaveform_start - current_PMT_trace_start
                            index_WaveForm_start = 0
                        if current_PMT_trace_end < CurrentWaveform_end:
                            index_POD_end = len(current_Pod_trace)
                            index_WaveForm_end = current_PMT_trace_end - CurrentWaveform_start
                        else:
                            index_POD_end = len(current_Pod_trace) - (current_PMT_trace_end - CurrentWaveform_end)
                            index_WaveForm_end = len(CurrentWaveform)

                        CurrentWaveform[int(index_WaveForm_start):int(index_WaveForm_end)] = current_Pod_trace[int(index_POD_start):int(index_POD_end)]
                    
                    S2_trace = self.trace_S2_filter(CurrentWaveform,S2width)
                    current_mul_prev = current_mul
                    current_mul = self.S2MultiplicityUpdate(S2_trace,S2Threshold,current_mul,S2Coinci)
                    for ChStore in range(len(current_mul)):
                            if(current_mul[ChStore]!=current_mul_prev[ChStore]):
                                ChannelAllRecords[ChStore].append(PMTs[j])
                    
                    
            CaptureInd = np.argwhere(current_mul>=S2Mulexpected)
            if(len(CaptureInd)==0):
                S2multiplicity.append(max(current_mul))
                CaptureInd = np.argmax(current_mul)
                ContributedChs.append(ChannelAllRecords[CaptureInd][1:])
            else:
                CaptureInd = CaptureInd[0][0]+CaptureWindow
                S2multiplicity.append(max(current_mul[:int(CaptureInd+1)]))
                ContributedChs.append(ChannelAllRecords[int(CaptureInd)][1:])
            
        return S2multiplicity,ContributedChs
            

    def HitVectorBinary(self,s2hits):
        # convert S2 hit vector into binary which corresponds to the 32 digitizers
        # The ROOT array loads the uint32 in the native system byte order (most popular is little-endian)

        S1HitVectors_ROOT = np.asarray(s2hits, dtype=np.uint32)
        # We need a big-endian version of the above array
        S1HitVectors_BEuint32 = S1HitVectors_ROOT.astype(dtype='>u4')
        # Extract the bit vectors from the big-endian array
        # Note that we reverse the order of the individual bit-vectors so that 0idx corresponds to DDC Ch0 and 31idx corresponds to DDC Ch31
        s2hits = np.flip(np.unpackbits(S1HitVectors_BEuint32.view('uint8')).reshape(len(S1HitVectors_BEuint32), 32),
                         axis=1)

        return s2hits

    def trace_S1_filter(self,PMT_trace, width):

        # Make a copy of the trace for the S1 filter
        S1_PMT_trace = 0.0 * PMT_trace

        # Create filter with specific width
        S1filter = [0.5] * width + [-1] * width + [0.5] * width

        # Apply filter to pulse to get the S1 trace
        S1_PMT_trace = np.convolve(PMT_trace, S1filter, 'valid')

        return S1_PMT_trace

    def S1MultiplicityUpdate(self,filterTrace, triggerTime, pulseTime, thre, mularray, coinw, timeWin, sampleAdd):

        # function to count and update multiplicity array for each trace
        # calculate where the first element located in the mul array
        index = (pulseTime - sampleAdd) - triggerTime

        if (index < 0):
            index = (pulseTime - sampleAdd) - triggerTime + timeWin

        # add 1 to mul array for each element above the threshold and the follow coinw elements
        if (0 < index < timeWin and (len(filterTrace) + index + coinw - 1) < int(timeWin * 2)):
            AboveTh = np.zeros(len(filterTrace))
            AboveTh[filterTrace > (thre - 0.5)] = 1
            AboveTh = np.convolve(AboveTh, np.ones(coinw))
            AboveTh = np.where(AboveTh > 0.5, 1, 0)

            mul = [0.] * int(index) + list(AboveTh) + [0.] * int((timeWin * 2) - index - len(AboveTh))
            # print(len(mul),index,len(AboveTh),len(filterTrace))

            # update the multiplicity array based on current trace
            mularray = np.array(mularray) + np.array(mul)

        elif (0 < index < timeWin and (len(filterTrace) + index + coinw - 1) > int(timeWin * 2)):
            AboveTh = np.zeros(len(filterTrace))
            AboveTh[filterTrace > (thre - 0.5)] = 1
            AboveTh = np.convolve(AboveTh, np.ones(coinw))
            AboveTh = np.where(AboveTh > 0.5, 1, 0)

            mul = [0.] * int(index) + list(AboveTh[:(400 - index)])
            # print(len(mul),index,len(AboveTh),len(filterTrace))

            # update the multiplicity array based on current trace
            mularray = np.array(mularray) + np.array(mul)

        return mularray

    def trace_S2_filter(self,PMT_trace, width):

        # function to obtain trace after the waveform go through the S2 filter
        # Input parameters:
        # PMT_trace: trace of the pod from the pmt
        # S2width = width of the central lobe of the S2 filter.
        # --YQie, Nov. 2021

        # Make a copy of the trace for the S2 filter
        S2_PMT_trace = 0.0 * np.array(PMT_trace)

        # Create filter with specific width
        S2filter = [2] * int(width) + [-1] * int(width * 4) + [2] * int(width)

        # Apply filter to pulse to get the S2 trace
        S2_PMT_trace = np.convolve(PMT_trace, S2filter, 'valid')

        return S2_PMT_trace

    def S2MultiplicityUpdate(self,filterTrace, thre, mularray, coinw):

        # function to count and update multiplicity array for each trace
        # Input parameters:
        # filterTrace = trace of the waveform after going through the filter
        # threshold = S2 threshold
        # mularray = multiplicity array for the event
        # coinw = length of coincidence window considered for multiplicity counting
        # --YQie, Nov. 2021

        # add 1 to multiplicity array for each element above the threshold and the following samples
        # the number of the following samples is determined by the concidence window
        AboveTh = np.zeros(len(filterTrace))
        AboveTh[filterTrace > (thre - 0.5)] = 1
        AboveTh = np.convolve(AboveTh, np.ones(coinw))
        AboveTh = np.where(AboveTh > 0.5, 1, 0)

        # update the mularray
        mularray = np.array(mularray) + np.array(AboveTh)

        return mularray